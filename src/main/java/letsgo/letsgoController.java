package letsgo;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class letsgoController {

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String getMainSite(){
        return "index";
    }
}
