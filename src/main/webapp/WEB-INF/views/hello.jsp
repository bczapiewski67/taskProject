<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <title>Niezła Strona</title>
</head>
<body>
    <h1>Witaj ${name}!</h1>
    <c:if test="${name == 'Bartek'}">
        <p>(swoja droga piekne imie)</p>
    </c:if>
    <h2>Oto moja strona</h2>
    <h2>Dodaj zadanie do zrobienia:</h2>

    <form:form action="tasks" modelAttribute="task" method="post">
        Task id<br>
        <form:input path="id" id="taskId"></form:input><br>
        <c:if test="${pageContext.request.method=='POST'}">
            <form:errors path="id" />
        </c:if>

        Task name<br>
        <form:input path="name" id="taskName"></form:input><br>
        <c:if test="${pageContext.request.method=='POST'}">
            <form:errors path="name" />
        </c:if>

        Task description<br>
        <form:input path="description" id="taskDescription"></form:input><br><br>
        <c:if test="${pageContext.request.method=='POST'}">
            <form:errors path="description" />
        </c:if>

        <input type="submit" value="Dodaj zadanie">
    </form:form>

</body>
</html>